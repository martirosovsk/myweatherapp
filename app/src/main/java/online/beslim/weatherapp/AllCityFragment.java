package online.beslim.weatherapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.annimon.stream.Stream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AllCityFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    public final static String CITY_LIST = "city_list";
    private final static String SHOW_ONLY_FAVORITES = "showOnlyFavorites";
    private final static String SORT_MODE = "sortMode";
    private final static int SORT_BY_TEMPERATURE = 1;
    private final static int SORT_BY_NAME = 0;
    private int sortMode = 0;
    private boolean isShowOnlyFavorites;
    private List<OneCity> allCity;
    private List<OneCity> allCityForAdapter;
    private Comparator<OneCity> oneCityComparatorByName = (city1, city2) -> city1.getName().compareTo(city2.getName());
    private Comparator<OneCity> oneCityComparatorByTemperature = (city1, city2) -> ((int) (city2.getTemperature() - city1.getTemperature()));
    private AllCityAdapter allCityAdapter;
    private String searchString;


    public AllCityFragment() {
        allCity = new ArrayList<>();
        allCityForAdapter = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isShowOnlyFavorites = MyWeatherAppActivity.settingsPref.getBoolean(SHOW_ONLY_FAVORITES, false);
        sortMode = MyWeatherAppActivity.settingsPref.getInt(SORT_MODE, SORT_BY_NAME);
        makeList();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_city, container, false);
//        Toast.makeText(getActivity(), "AllFA " + allCityForAdapter.size(), Toast.LENGTH_SHORT).show();
        allCityAdapter = new AllCityAdapter(allCityForAdapter, this::onButtonPressed);
        RecyclerView allCityList = view.findViewById(R.id.fragment_all_city_list);
        allCityList.setLayoutManager(new LinearLayoutManager(getContext()));
        allCityList.setAdapter(allCityAdapter);
//        if (searchString != null){search(searchString);}
        return view;
    }

    public void onButtonPressed(int cityId) {
        if (mListener != null) {
            mListener.onFragmentInteraction(allCity.indexOf(allCityForAdapter.get(cityId))); // ох и долго я искал, почему не в тот город попадает
        }
    }

    public void setListener(OnFragmentInteractionListener listener) {
        this.mListener = listener;
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        if (args != null) {
            Serializable collection = args.getSerializable(CITY_LIST);
            if (collection instanceof List) {
                if (!((List) collection).isEmpty() && ((List) collection).get(0) instanceof OneCity) {
                    List<OneCity> cityList = (List<OneCity>) collection;
                    allCity.clear();
                    allCity.addAll(cityList);
                }
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int cityId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.all_city_fragment_menu, menu);
        MenuItem isShowFavoriteItem = menu.findItem(R.id.show_favorites);
        isShowFavoriteItem.setChecked(isShowOnlyFavorites);
        MenuItem sortByTemperatureItem = menu.findItem(R.id.sort_by_temperature);
        MenuItem sortByNameItem = menu.findItem(R.id.sort_name);
        if (sortMode == SORT_BY_TEMPERATURE) {
            sortByTemperatureItem.setChecked(true);
        }
        if (sortMode == SORT_BY_NAME) {
            sortByNameItem.setChecked(true);
        }

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchText = (SearchView) search.getActionView();
        if (!searchText.isIconified()) {
            Toast.makeText(getActivity(), "is", Toast.LENGTH_SHORT).show();
            search(searchText.getQuery().toString());
        }
        searchText.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!s.isEmpty()) {
                    searchText.clearFocus();
                    searchString = s;
                    searchCity(s);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!s.isEmpty()) {
                    search(s);
                } else {
                    refresh();
                }
                return true;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_favorites:
                return showFavorites(item);
            case R.id.sort_by_temperature:
                return sortByTemperature(item);
            case R.id.sort_name:
                return sortByName(item);
            case R.id.refresh:
                return refresh();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean sortByName(MenuItem item) {
        item.setChecked(true);
        sortMode = SORT_BY_NAME;
        refresh();
        MyWeatherAppActivity.settingsPref.edit().putInt(SORT_MODE, SORT_BY_NAME).apply();
        toast(R.string.sorted_by_name);
        return true;
    }

    private boolean sortByTemperature(MenuItem item) {
        item.setChecked(true);
        sortMode = SORT_BY_TEMPERATURE;
        refresh();
        MyWeatherAppActivity.settingsPref.edit().putInt(SORT_MODE, SORT_BY_TEMPERATURE).apply();
        toast(R.string.sorted_by_temperature);
        return true;
    }

    private boolean refresh() {
        makeList();
        allCityAdapter.notifyDataSetChanged();
        return true;
    }

    private void makeList() {
//        Toast.makeText(getActivity(), "makeList "+allCity.size(), Toast.LENGTH_SHORT).show();
        allCityForAdapter.clear();
        allCityForAdapter.addAll(isShowOnlyFavorites ? filterOnlyFavorite(allCity) : allCity);
        Collections.sort(allCityForAdapter, (sortMode == SORT_BY_NAME) ? oneCityComparatorByName : oneCityComparatorByTemperature);
    }

    private List<OneCity> filterOnlyFavorite(List<OneCity> list) {
        return Stream.of(list).filter(p -> p.isFavorite()).collect(com.annimon.stream.Collectors.toList());
    }

    public boolean search(String s) {

        allCityForAdapter.clear();
        allCityForAdapter.addAll(
                Stream.of(allCity)
                        .filter(p -> p.getName().toUpperCase().startsWith(s.toUpperCase()))
                        .collect(com.annimon.stream.Collectors.toList()));
        allCityAdapter.notifyDataSetChanged();
//        toast(R.string.search);
        return true;
    }

    public boolean searchCity(String s) {
        MyCustomTask task = new MyCustomTask(((MyWeatherAppActivity) getActivity()).allCity, (MyWeatherAppActivity) getActivity());
        task.execute(s);
        return true;
    }

    public boolean showFavorites(MenuItem item) {
        allCityForAdapter.clear();
        if (!item.isChecked()) {
            item.setChecked(true);
            isShowOnlyFavorites = true;
            refresh();
            MyWeatherAppActivity.settingsPref.edit().putBoolean(SHOW_ONLY_FAVORITES, true).apply();
            toast(R.string.favorites);
        } else {
            item.setChecked(false);
            isShowOnlyFavorites = false;
            refresh();
            MyWeatherAppActivity.settingsPref.edit().putBoolean(SHOW_ONLY_FAVORITES, false).apply();
            toast(R.string.showed_all);
        }
        return true;
    }

    public void toast(int resID) {
        Toast.makeText(getActivity(), resID, Toast.LENGTH_SHORT).show();
    }
}
