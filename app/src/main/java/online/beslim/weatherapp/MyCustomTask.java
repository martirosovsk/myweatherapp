package online.beslim.weatherapp;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyCustomTask extends AsyncTask<String, String, String> {
    private List<OneCity> allCityList;
    private MyWeatherAppActivity appActivity;
    private OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(getAPPIDInterceptor())
            .build();
    private Retrofit retrofit;

    public MyCustomTask(List<OneCity> allCityList, MyWeatherAppActivity appActivity) {
        this.allCityList = allCityList;
        this.appActivity = appActivity;
    }

    public static final String MY_CUSTOM_TASK = "MyCustomTask";

    @Override
    protected String doInBackground(String... objects) {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        OpenWeatherMap openWeatherMap = retrofit.create(OpenWeatherMap.class);
        for (String s : objects) {
            try {
                Call<OneCity> call = openWeatherMap.getWeatherByCityName(s);
                Response<OneCity> response = call.execute();
                if (response.code() != 200) {
                    publishProgress(response.message());
                    continue;
                }
                OneCity city = response.body();
                if (allCityList.contains(city)) {
                    continue;
                }
                allCityList.add(city);
                Log.d(MY_CUSTOM_TASK, "doInBackGround: " + city.toString());

            } catch (IOException e) {
                e.printStackTrace();
            }
//            publishProgress(s);
        }
        return "onPostRock";
    }

    @Override
    protected void onPostExecute(String s) {
        Log.d(MY_CUSTOM_TASK, "doOnPostExecute: " + s);
//        Toast.makeText(App.getInstance(), "Total list after task: " + allCityList.size(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(App.getInstance(), getStatus().name(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(App.getInstance(), "Total list after task: " + allCityList.size(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(App.getInstance(), "AllCity: " + appActivity.isListEmpty(), Toast.LENGTH_SHORT).show();
        appActivity.replaceWithAllCityFragment();
        super.onPostExecute(s);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Log.d(MY_CUSTOM_TASK, "doOnProgressUpdate : " + values[0]);
        Toast.makeText(App.getInstance(), values[0], Toast.LENGTH_SHORT).show();

        super.onProgressUpdate(values);
    }

    private Interceptor getAPPIDInterceptor() {
        return chain -> {
            Request request = chain.request();
            HttpUrl url = request.url();
            url = url.newBuilder().addQueryParameter("APPID", "b2cf043dae6ffd31238ef558401cf6e3").build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        };
    }
}
