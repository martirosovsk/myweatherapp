package online.beslim.weatherapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AllCityAdapter extends RecyclerView.Adapter<AllCityAdapter.ViewHolder> {

    private final List<OneCity> allCity;
    private final OnCityClickListener listener;

    AllCityAdapter(List<OneCity> allCity, OnCityClickListener listener) {
        this.listener = listener;
        this.allCity = allCity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.one_city, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        OneCity oneCity = allCity.get(i);
        viewHolder.cityLabel.setText(oneCity.getName());

        StringBuilder iconURL = new StringBuilder("http://openweathermap.org/img/w/").append(oneCity.getIcon()).append(".png");
        GlideApp
                .with(App.getInstance())
                .load(iconURL.toString())
                .placeholder(R.drawable.ic_do_not_not_available_24dp)
                .into(viewHolder.cityCoatOfArm);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(i);
            }
        });

        viewHolder.cityTemperature.setText(oneCity.getTemperatureAsString());
        viewHolder.cityHumidity.setText(oneCity.getHumidity());
    }


    @Override
    public int getItemCount() {
        return allCity.size();
    }

    public interface OnCityClickListener {
        void onClick(int cityId);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cityCoatOfArm;
        TextView cityLabel;
        TextView cityTemperature;
        TextView cityHumidity;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            cityLabel = itemView.findViewById(R.id.one_city_label);
            cityTemperature = itemView.findViewById(R.id.one_city_temperature);
            cityHumidity = itemView.findViewById(R.id.one_city_humidity);
            cityCoatOfArm = itemView.findViewById(R.id.one_city_image);
        }
    }


}
