package online.beslim.weatherapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;


public class MyCustomView extends View {
    private String cityName;
    private int cityTemperatureColor = Color.RED;
    private int textColor;
    private float textDimension = 0;
    private Drawable cityDrawable;
    private String cityTemperature;
    private String cityHumidity;
    private TextPaint textPaint;
    private Paint paintImage;


    public MyCustomView(Context context) {
        super(context);
        init(null, 0);
    }

    public MyCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MyCustomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.MyCustomView, defStyle, 0);
        textColor = a.getColor(R.styleable.MyCustomView_textColor, textColor);
        textDimension = a.getDimension(
                R.styleable.MyCustomView_textDimension,
                textDimension);
        a.recycle();
        textPaint = new TextPaint();
        textPaint.setTextSize(textDimension);
        textPaint.setColor(textColor);
        paintImage = new Paint();
        paintImage.setAlpha(120);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        canvas.drawBitmap(((BitmapDrawable) cityDrawable).getBitmap(), null, new Rect(64, 64, 964, 964), paintImage);
        canvas.drawText(cityName, 16, 100, textPaint);
        canvas.drawText(cityTemperature, 16, 200, textPaint);
        canvas.drawText(cityHumidity, 16, 300, textPaint);
    }

    public void setCityName(String exampleString) {
        cityName = exampleString;
    }

    public void setCityTemperatureColor(int exampleColor) {
        cityTemperatureColor = exampleColor;
    }

    public void setCityDrawable(Drawable exampleDrawable) {
        cityDrawable = exampleDrawable;
    }

    public void setCityTemperature(String cityTemperature) {
        this.cityTemperature = cityTemperature;
    }

    public void setCityHumidity(String cityHumidity) {
        this.cityHumidity = cityHumidity;
    }
}
