package online.beslim.weatherapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherMap {
    @GET("weather")
    Call<OneCity> getWeatherByCityName(@Query("q") String s);
}
