package online.beslim.weatherapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class MyWeatherAppActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public final List<OneCity> allCity;
    private AllCityFragment allCityFragment;
    private OneCityFragment oneCityFragment;
    public static final String FAVORITE_CITIES_PREF = "FavoriteCities";
    public static final String SETTINGS_PREF = "Settings";
    public static SharedPreferences favoriteCitiesPref;
    public static SharedPreferences settingsPref;
    private TextView currentRoomTemperature;
    private SensorManager sensorManager;
    private Sensor sensorTemperature;
    private SensorEventListener sensorEventListener;
    private final String MY_KEY = "MyWeatherAppActivity";
    private List<Sensor> sensorList;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FloatingActionButton fab;


    public MyWeatherAppActivity() {
        allCity = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        settingsPref = getSharedPreferences(SETTINGS_PREF, MODE_PRIVATE);
        favoriteCitiesPref = getSharedPreferences(FAVORITE_CITIES_PREF, MODE_PRIVATE);
        initCityList();
        initUi();
        initSensor();
    }

    private void initSensor() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        sensorTemperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                currentRoomTemperature.setText(MessageFormat.format(getString(R.string.rt), (int) event.values[0]));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        sensorManager.registerListener(sensorEventListener, sensorTemperature, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void initCityList() {
//        Resources resources = getResources();
//        allCity.add(new OneCity(resources.getString(R.string.title_nsk), resources.getString(R.string.description_nsk), -5));
//        allCity.add(new OneCity(resources.getString(R.string.title_msk), resources.getString(R.string.description_msk), -10));
//        allCity.add(new OneCity(resources.getString(R.string.title_spb), resources.getString(R.string.description_spb), 0));
        MyCustomTask customTask = new MyCustomTask(allCity, this);
        customTask.execute("Moscow", "Novosibirsk", "Saint Petersburg");
        for (OneCity city : allCity) {
            city.setFavorite(favoriteCitiesPref.getBoolean(city.getName(), false));
        }
    }

    private void initUi() {
        currentRoomTemperature = findViewById(R.id.room_temperature);
        allCityFragment = new AllCityFragment();
        oneCityFragment = new OneCityFragment();


        fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        replaceWithAllCityFragment();

    }

    public void replaceWithAllCityFragment() {
//        Toast.makeText(this, allCity.size(), Toast.LENGTH_SHORT).show();

        Bundle bundle = new Bundle();
        bundle.putSerializable(AllCityFragment.CITY_LIST, (Serializable) allCity);
        allCityFragment.setArguments(bundle);
        allCityFragment.setListener(new AllCityFragment.OnFragmentInteractionListener() {
            @Override
            public void onFragmentInteraction(int id) {
                MyWeatherAppActivity.this.replaceWithOneCityFragment(id);
            }
        });
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.navigation_activity_container, allCityFragment)
                .commit();
    }

    private void replaceWithOneCityFragment(int id) {
        Bundle args = new Bundle();
        args.putSerializable(OneCityFragment.CITY_ID, allCity.get(id));
        oneCityFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.navigation_activity_container, oneCityFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (oneCityFragment.isAdded()) {
            replaceWithAllCityFragment();
            return;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                return settings();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_camera:
                break;
            case R.id.nav_async_task:
                MyCustomTask customTask = new MyCustomTask(allCity, this);
                customTask.execute("Moscow", "Novosibirsk", "Saint Petersburg");
                break;
            case R.id.nav_my_custom_service:
                Intent intent = new Intent(this, MyIntentService.class);
                intent.putExtra(MY_KEY, "value");
                startService(intent);
                Toast.makeText(this, "post service started: ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_manage:
                break;
            case R.id.nav_share:
                break;
            case R.id.nav_send:
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean settings() {
        toast(R.string.settings);
        return true;
    }

    public void toast(int resID) {
        Toast.makeText(this, resID, Toast.LENGTH_SHORT).show();
    }

    public String isListEmpty() {
        if (allCity.isEmpty()) {
            return "Empty";
        } else {
            return String.valueOf(allCity.size());
        }
    }

}