package online.beslim.weatherapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.text.MessageFormat;

public class OneCityFragment extends Fragment {

    public static final String CITY_ID = "city_name";
    private OneCity city;
    private MyCustomView customView;

    public OneCityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one_city, container, false);
        customView = view.findViewById(R.id.customView);
        customView.setCityName(city.getName());

        customView.setCityDrawable(getActivity().getDrawable(App.getImage(city.getName())));
        customView.setCityTemperature(MessageFormat.format(getString(R.string.city_temperature), city.getTemperatureAsString()));
        customView.setCityHumidity(MessageFormat.format(getString(R.string.city_humidity), city.getHumidity()));
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle bundle = getArguments();
        if (bundle != null) {
            city = (OneCity) bundle.getSerializable(CITY_ID);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.context_menu_one_city_fragment, menu);
        MenuItem addToFavorite = menu.findItem(R.id.add_to_favorites);
        addToFavorite.setIcon(city.isFavorite() ? R.drawable.ic_star_black_24dp : R.drawable.ic_star_border_24dp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_to_favorites:
                return setFavorite(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean setFavorite(MenuItem item) {
        if (city.isFavorite()) {
            city.setFavorite(false);
            saveFavorite(city.getName(), false);
            item.setIcon(R.drawable.ic_star_border_24dp);

        } else {
            saveFavorite(city.getName(), true);
            city.setFavorite(true);
            item.setIcon(R.drawable.ic_star_black_24dp);
        }
        return true;
    }

    private void saveFavorite(String cityName, boolean isFavorite) {
        MyWeatherAppActivity.favoriteCitiesPref.edit().putBoolean(cityName, isFavorite).apply();
    }
}
