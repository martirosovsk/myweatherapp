package online.beslim.weatherapp;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OneCity implements Serializable {

    private final String name;
    private final String description;
    private float temperature;
    private int humidity;
    private boolean isFavorite;
    @SerializedName("coord")
    private Coordinates coordinates;
    @SerializedName("weather")
    private List<WeatherOne> weatherOneList;
    @SerializedName("main")
    private WeatherMain weatherMain;
    private int cod;
    private int id;

    public int getId() {
        return id;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }


    OneCity(String name, String description, int temperature) {
        this.name = name;
        this.description = description;
        this.temperature = temperature;
    }

    public String getHumidity() {
        return "Hum: " + weatherMain.humidity + "%";
    }

    public String getName() {
        return name;
    }

    public String getTemperatureAsString() {
        return "Temp: " + String.format("%,.1f", (weatherMain.temp - 273.15)) + "°C";
    }

    public float getTemperature() {
        return weatherMain.temp;
    }

    public String getIcon() {
        return weatherOneList.get(0).icon;
    }

    @Override
    public String toString() {
        return name + " " + weatherMain.temp + " " + getIcon();
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj instanceof OneCity) && (((OneCity) obj).getId() == this.getId())) {
            return true;
        } else {
            return false;
        }
    }

    private class Coordinates {
        private float longitude;
        private float latitude;
    }

    private class WeatherOne {
        private int id;
        private String main;
        private String description;
        private String icon;

    }

    private class WeatherMain {
        float temp;
        float pressure;
        float humidity;
    }

    private class Wind {
        private float speed;
        private float deg;
    }

    private class Clouds {
        private int all;
    }
}
